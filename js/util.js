const _ = require("underscore");


function Vector(x, y) {
	this.x = x;
	this.y = y;
}
Vector.prototype = {
	Coord: function(l) { return this.x + l * this.y; },
	Clone: function() { return new Vector(this.x, this.y); },
	ManhattanLength: function() { return Math.abs(this.x) + Math.abs(this.y); },
	ManhattanDistance: function (other) { return Math.abs(this.x - other.x) + Math.abs(this.y - other.y); },
	Equals: function(other) { return this.x == other.x && this.y == other.y; },
	Add: function(other) { return new Vector(this.x+other.x, this.y+other.y); },
	Subtract: function(other) { return new Vector(this.x-other.x, this.y-other.y); }
}

function VecFromIndex(index, sl) {
	return new Vector(index%sl, Math.floor(index/sl));
}

function Color(r, g, b) {
	this.r = r;
	this.g = g;
	this.b = b;
}
Color.prototype = {
	ToCSS: function() { return "rgb("+Math.round(this.r)+","+Math.round(this.g)+","+Math.round(this.b)+")"; }
}

function Lerp(x, a, b) {
	return (1-x)*a + x*b;
}

function MixColors(x, a, b) {
	return new Color(Lerp(x, a.r, b.r), Lerp(x, a.g, b.g), Lerp(x, a.b, b.b));
}

function DecToHex(num) {
	return ("0"+Math.round(num).toString(16)).substr(-2);
}

function Clamp(x, min, max) {
	return Math.min(Math.max(x, min), max);
}

function Clamp2(x, a, b) {
	return Clamp(x, Math.min(a,b), Math.max(a,b));
}

function LinearScale(x, in_min, in_max, out_min, out_max) {
	return Clamp2((Clamp2(x, in_min, in_max) - in_min) * (out_max - out_min) / (in_max - in_min) + out_min, out_min, out_max);
}

function CreateMap(sl) {
	return _.flatten(_.map(_.range(sl), y => _.map(_.range(sl), x => new Vector(x,y) )), true);
}

function ArePointsOnAxis(a, b) {
	return Math.abs(a.x-b.x) * Math.abs(a.y-b.y) == 0;
}

function rotate(arr, first = 0, n_first = 1, last = arr.length) {
	let next = n_first;
    while (first != next) {
		let tmp = arr[first];
		arr[first] = arr[next];
		arr[next] = tmp;
        first++; next++;
        if (next == last) {
            next = n_first;
        } else if (first == n_first) {
            n_first = next;
        }
    }
}

function AdvancePointToOtherPoint(a, b) {
	return new Vector(a.x + Math.sign(b.x-a.x), a.y + Math.sign(b.y-a.y));
}

function Inside(x, a, b) {
	return x >= Math.min(a, b) && x <= Math.max(a, b);
}

module.exports = {
	Point: Vector,
	Vector: Vector,
	VecFromIndex: VecFromIndex,
	DecToHex: DecToHex,
	Clamp: Clamp,
	Clamp2: Clamp2,
	LinearScale: LinearScale,
	CreateMap: CreateMap,
	ArePointsOnAxis: ArePointsOnAxis,
	rotate: rotate,
	AdvancePointToOtherPoint: AdvancePointToOtherPoint,
	Inside: Inside,
	Color: Color,
	Lerp: Lerp,
	MixColors: MixColors
}
