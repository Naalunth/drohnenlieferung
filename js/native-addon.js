const native = require("../build/Release/addon");
const city = require("./city");
const _ = require("underscore");
const util = require("./util");
const Vector = util.Vector;

function CalculateSolution(c, cb) {
	var handle_ = native.StartSolutionWorker(c, (err, res, done) => {
		if (err) console.log(err, null);
		console.log(res);
		var solution = _.map(res, s => {
			var step = new city.Step(c.sideLength);
			_.each(s, o => {
				step.AddTransfer(new Vector(o.from.x, o.from.y), new Vector(o.to.x, o.to.y));
			});
			return step;
		});
		cb(err, solution, done);
	});
	return handle_;
}

module.exports = {
	CalculateSolution: CalculateSolution
};
