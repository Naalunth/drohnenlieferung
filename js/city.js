const _ = require("underscore");
const util = require("./util");
const Vector = util.Vector;


function City(sideLength) {
	this.sideLength = sideLength;
	this.data = util.CreateMap(sideLength);
}
City.prototype = {
	test: function() {
		return this.data;
	},

	ApplyStep: function(step) {
		if (!step.IsValid()) return;
		let newCity = this.Clone();
		_.each(step.transfers, t => newCity.data[t.to.Coord(this.sideLength)] = this.data[t.from.Coord(this.sideLength)].Clone());
		return newCity;
	},

	Clone: function() {
		let n = new City(this.sideLength);
		n.data = this.data.slice(0);
		return n;
	},

	Deviation: function() {
		return _.reduce(util.CreateMap(this.sideLength), (memo,p) => {
			return this.data[p.Coord(this.sideLength)].ManhattanDistance(p) + memo;
		}, 0);
	}
};


function Step(sideLength) {
	this.sideLength = sideLength;
	this.transfers = [];
}
Step.prototype = {
	AddTransfer: function(from, to) {
		if (!from.Equals(to)) {
			this.transfers.push({from: from.Clone(), to: to.Clone()});
		}
	},

	IsValid: function() {
		let sums = _.map(_.range(this.sideLength*this.sideLength), r => 0);
		_.each(this.transfers, t => {++sums[t.from.Coord(this.sideLength)]; ++sums[t.to.Coord(this.sideLength)];});
		return _.every(sums, x => x == 0 || x == 2);
	},

	Inverted: function() {
		let s = new Step(this.sideLength);
		s.transfers = _.map(this.transfers, t => ({from: t.to, to: t.from}));
		return s;
	}
};


function Loop(corners) {
	this.corners = corners;
	this.Normalize();
}
Loop.prototype = {
	IsValid: function() {
		if (this.corners.length < 2) return false;
		//if (_.every(_.zip(this.corners, util.rotate(this.corners)), x => {return util.ArePointsOnAxis(x[0], x[1]) && )) return false;
		return true;
	},

	Clone: function() {
		return new Loop(_.map(this.corners, c => c.Clone()));
	},

	Reverse: function() {
		this.corners.reverse();
		return this;
	},

	Normalize: function() {
		let c = this.corners;
		let cl = () => c.length;
		let cit = 0;
		let ncit = () => (cit+1) % cl();
		while (cit < cl()) {
			if (!util.ArePointsOnAxis(c[cit], c[ncit()]))
				c.splice(cit + 1, 0, new Vector(c[cit].x, c[ncit()].y));
			while (cl() > cit && c[cit].Equals(c[ncit()]))
				c.splice(ncit(), 1);
			cit++;
		}
	}
}


function AbstractStep(sideLength) {
	this.sideLength = sideLength;
	this.loops = [];
	this.step = {};
}
AbstractStep.prototype = {
	AddLoop: function(loop) {
		if (loop.IsValid()) {
			this.loops.push(loop);
		}
	},

	IsValid: function() {
		this.step = this.ToStep();
		return this.step.IsValid();
	},

	ToStep: function() {
		let step = new Step(this.sideLength);
		_.each(this.loops, l => {
			let it = l.corners[0];
			let cit = 1;
			do {
				let next = util.AdvancePointToOtherPoint(it, l.corners[cit]);
				step.AddTransfer(it, next);
				while (next.Equals(l.corners[cit]) && cit!=0) cit = (cit + 1) % l.corners.length;
				it = next;
			} while (!it.Equals(l.corners[0]));
		});
		return step;
	},

	Inverted: function() {
		let a = new AbstractStep(this.sideLength);
		_.each(this.loops, l => a.AddLoop(l.Clone().Reverse()));
		return a;
	}
};


module.exports = {
	Vector: Vector,
	City: City,
	Step: Step,
	Loop: Loop,
	AbstractStep: AbstractStep
};
