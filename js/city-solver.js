const _ = require("underscore");
const city = require("./city");
const util = require("./util");
const Vector = util.Vector;


function* StepGuessGenerator(c) {
	var neutralMap = util.CreateMap(c.sideLength);
	let sl = c.sideLength;
	let relMap = _.map(neutralMap, p => {
		return { position: p, rel: c.data[p.Coord(sl)].Subtract(p) };
	});

	let neededTransfers = _.sortBy(_.shuffle(_.compact(_.map(relMap, re => {
		let transfers = [];
		if (re.rel.x != 0) transfers.push( { from: re.position, to: re.position.Add(new Vector(Math.sign(re.rel.x), 0)) } );
		if (re.rel.y != 0) transfers.push( { from: re.position, to: re.position.Add(new Vector(0, Math.sign(re.rel.y))) } );
		if (transfers.length == 2 && Math.abs(re.rel.y) > Math.abs(re.rel.x)) {
			let tmp = transfers[0];
			transfers[0] = transfers[1];
			transfers[1] = tmp;
		}
		if (transfers.length == 2 && Math.random() >= 0.6) {
			let tmp = transfers[0];
			transfers[0] = transfers[1];
			transfers[1] = tmp;
		}
		_.compact(transfers);
		if (transfers.length == 0) return null;
		return { transfers: transfers, priority: re.rel.ManhattanLength() };
	}))), nt => -nt.priority+2*sl);

	let time = Date.now();
	let bestResult = null;
	let constraints = [];
	for (let i = 0; i < Math.min(neededTransfers.length, 100); i++) {
		constraints.push(neededTransfers[i].transfers[0]);
		let result = ResolveStepConstraints(c, constraints);
		if (result.success) bestResult = result;
		else constraints.pop();
		if ((Date.now() - time) > 2000 && bestResult != null) yield bestResult.step;
	}
	if (bestResult != null) yield bestResult.step;

	return;
}

function ResolveStepConstraints(c, constraints) {
	let sl = c.sideLength;
	var neutralMap = util.CreateMap(sl);

	let invalidReturn = { success: false, step: null };

	let NewSlot = function(p) {
		this.position = p;
		this.packet = null;
		this.fixed = false;
		this.ns = true;
		this.markedForFreeing = false;
	}

	NewSlot.prototype.ClaimPacket = function(os, fix) {
		if (fix == undefined) fix = false;
		if (os.packet != null && !this.fixed && !os.fixed && !os.ns) {
			let oldPacket = this.packet;
			this.packet = os.packet;
			os.packet = null;
			this.packet.owner = this;
			this.fixed = fix;
			os.fixed = fix;
			return { success: true, packet: oldPacket };
		}
		return {success: false};
	}

	let OldSlot = function(p, packet) {
		this.position = p;
		this.packet = packet;
		this.formerPacket = packet;
		this.fixed = false;
		this.ns = false;
		packet.owner = this;
	}

	let Packet = function(p) {
		this.from = p;
		this.owner = null;
	}

	let OldSlots = _.map(neutralMap, p => new OldSlot(p, new Packet(p)) );
	let NewSlots = _.map(neutralMap, p => new NewSlot(p) );


	// Apply constraints

	if (!_.every(constraints, cons => {
		let os = OldSlots[cons.from.Coord(sl)];
		let ns = NewSlots[cons.to.Coord(sl)];
		if (cons.from.ManhattanDistance(cons.to) > 1) return false;
		let result = ns.ClaimPacket(os, true);
		if (!result.success || result.packet != null) return false;
		return true;
	})) return invalidReturn;


	// Single out cases, where there is only one possible packet configuration

	let counter;
	do {
		counter = 0;
		if (!_.every(NewSlots, ns => {
			if (ns.packet == null) {
				let o = GetPacketOptions(ns.position, OldSlots, sl);
				if (o.count == 0)
					return false;
				if (o.count == 1) {
					let os = o.options[0];
					ns.ClaimPacket(os, true);
					counter++;
				}
			}
			return true;
		})) return invalidReturn;
	} while (counter != 0);


	// Some Utility

	let searchForPacket = function(startSlot) {
		_.each(NewSlots, ns => {ns.markedForFreeing = false;});
		startSlot.markedForFreeing = true;
		var sortPredicate = x => x.slot.packet.from.ManhattanDistance(startSlot.position);
		var searchQueue = [{ slot: startSlot, path: [] }];

		while (searchQueue.length > 0) {
			var attempt = searchQueue.shift();
			var slot = attempt.slot;
			slot.markedForFreeing = true;

			var options = GetPacketOptions(slot.position, OldSlots, sl);
			if (options.count > 0) {
				let op = slot.packet;
				slot.ClaimPacket(options.options[0]);
				return { success: true, path: attempt.path, packet: op };
			}

			var newPath = attempt.path.slice(0);
			newPath.push(slot);
			_.each(GetOldSlotsForFreeSearch(slot.position, OldSlots, sl), os => {
				var ns = os.formerPacket.owner;
				if (!ns.markedForFreeing) {
					var obj = { slot: os.formerPacket.owner, path: newPath };
					searchQueue.splice(_.sortedIndex(searchQueue, obj, sortPredicate), 0, obj);
				}
			})
		}
		return { success: false };
	}


	NewSlot.prototype.TryClaimPacket = function() {
		if (this.packet != null) return true;
		if (!this.fixed) {
			let result = searchForPacket(this);
			if (!result.success) return false;

			_.reduceRight(result.path, (pck, ns) => {
				let op = ns.packet;
				ns.packet = pck;
				ns.packet.owner = ns;
				return op;
			}, result.packet);
			return true;
		}
		return false;
	}


	// Solve all the remaining holes

	if (!_.every(NewSlots, ns => ns.TryClaimPacket())) return invalidReturn;


	// Generate complete valid step

	let step = new city.Step(sl);
	_.each(OldSlots, os => {
		step.AddTransfer(os.position, os.formerPacket.owner.position);
	});

	return { success: true, step: step };
}



//---------------------------
// HIDDEN UTILITY FUNCTIONS
//---------------------------

function SearchCircleGenerator(p, radius, sl) {
	var result = [p];
	if (radius == 1) {
		if (p.x+1 < sl) result.push(new Vector(p.x+1, p.y));
		if (p.x-1 >= 0) result.push(new Vector(p.x-1, p.y));
		if (p.y+1 < sl) result.push(new Vector(p.x, p.y+1));
		if (p.y-1 >= 0) result.push(new Vector(p.x, p.y-1));
	} else {
		for (var i = 1; i <= radius; i++) {
			let x = i, y = 0, dx = -1, dy = 1;
			do {
				var np = p.Add(new Vector(x, y));
				if (util.Inside(np.x, 0, sl-1) && util.Inside(np.y, 0, sl-1)) {
					result.push(np);
				}
				x += dx; y += dy;
				if (Math.abs(x) == radius) dx *= -1;
				if (Math.abs(y) == radius) dy *= -1;
			} while (x != radius);
		}
	}
	return result;
}

function GetPacketOptions(p, oss, sl) {
	var counter = 0;
	var options = [];
	_.each(SearchCircleGenerator(p, 1, sl), candidate => {
		var os = oss[candidate.Coord(sl)];
		if (os.packet != null && !os.fixed) {
			options.push(os);
			counter++;
		}
	});
	return { count: counter, options: options };
}

function GetOldSlotsForFreeSearch(p, oss, sl) {
	var result = []
	_.each(SearchCircleGenerator(p, 1, sl), candidate => {
		var os = oss[candidate.Coord(sl)];
		if (!os.fixed && !os.markedForFreeing) {
			result.push(os);
		}
	});
	return result;
}


module.exports = {
	StepGuessGenerator: StepGuessGenerator,
	GetGoodNextStep: GetGoodNextStep
};
