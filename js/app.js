"use strict";

const fs = require("fs");
const _ = require("underscore");
const bootstrap = require("bootstrap");
const city = require("./city");
const util = require("./util");
const Vector = util.Vector;

var SegfaultHandler = require('segfault-handler');
SegfaultHandler.registerHandler("crash.log");

const native_solver = require("./native-addon");

const gridCellSize = 4.2;

$(() => {
	$(window).trigger("resize");
	loadedCity = new city.City(10);
	DrawCity(loadedCity);
	//*
	//*/
});



var loadedCity = null;
var generatedSolution = [];
var loadedSolution = [];
var shownCity = null;
var searching = false;
var searchHandle = null;

let tmot;
function AnimateSolution(sol) {
	AnimateTransition(shownCity, _.head(sol));
	if (sol.length > 1)
		tmot = setTimeout(AnimateSolution, 400, _.tail(sol));
};


function SetSearchButtonSearch() {
	$("#search-button").text("Search solution")
	.addClass("btn-primary").removeClass("btn-danger");
}
function SetSearchButtonStop() {
	$("#search-button").text("Stop")
	.removeClass("btn-primary").addClass("btn-danger");
}

$("#search-button").click(ev => {
	if (searching) {
		searching = false;
		SetSearchButtonSearch();
		$("#search-status").text("Ready");
		searchHandle.Stop();
	} else {
		searching = true;
		SetSearchButtonStop();
		$("#search-status").text("Searching...");
		searchHandle = native_solver.CalculateSolution(loadedCity, (err, solution, done) => {
			if (searching && !done) {
				$("#search-status").text("Found solution ("+solution.length+" steps)");
				generatedSolution = solution;
			}
		});
	}
});


$("#show-button").click(ev => {
	clearTimeout(tmot);
	if (searching)
		$("#search-status").text("Searching...");
	else
		$("#search-status").text("Ready");
	DrawCity(loadedCity);
	loadedSolution = generatedSolution;
	$("#show-status").text("Loaded solution ("+loadedSolution.length+" steps)");
	clearTimeout(tmot);
	tmot = setTimeout(AnimateSolution, 200, loadedSolution);
});

$("#save-button").click(ev => {
	var str = "";
	var sl = loadedCity.sideLength;
	var neutralMap = util.CreateMap(sl);
	var stepsByCoord = _.map(neutralMap, p => "");
	_.each(loadedSolution, step => {
		_.each(neutralMap, p => {
			var transfer = _.find(step.transfers, t => {
				return t.from.Equals(p);
			})
			var letter;
			if (!transfer) letter = "_";
			else {
				var dir = transfer.to.Subtract(transfer.from);
				if (dir.Equals(new Vector(1, 0))) letter = "O";
				if (dir.Equals(new Vector(-1, 0))) letter = "W";
				if (dir.Equals(new Vector(0, 1))) letter = "S";
				if (dir.Equals(new Vector(0, -1))) letter = "N";
			}
			stepsByCoord[p.Coord(sl)] += letter;
		});
	});
	_.each(neutralMap, p => {
		str += p.y + " " + p.x + " " + stepsByCoord[p.Coord(sl)] + "\n";
	});
	fs.writeFile("./solutions/"+$("#city-file-input")[0].files[0].name+".solution-"+loadedSolution.length+".txt", str, (err) => {
		if (err) console.log(err);
	});
});

$("#city-file-input").change(ev => {
	if (searching) {
		searching = false;
		SetSearchButtonSearch();
		$("#search-status").text("Ready");
		searchHandle.Stop();
	}
	clearTimeout(tmot);
	$("#show-status").text("");
	let reader = new FileReader();
	reader.onload = ev => {
		let arr = ev.target.result.split(/\r?\n/)
		let sl = parseInt(arr[0], 10);
		loadedCity = new city.City(sl);
		_.each(_.tail(arr), line => {
			let la = _.map(line.split(/\s+/), n => parseInt(n, 10));
			if (la.length < 4) return;
			loadedCity.data[(new Vector(la[1], la[0])).Coord(sl)] = new Vector(la[3], la[2]);
		});
		DrawCity(loadedCity);
		console.log(loadedCity.Deviation());
	};
	reader.readAsText(ev.target.files[0]);
});

$(window).resize(()=>{/*
	var gw = $(".grid-wrapper");
	var size = Math.min(gw.height(), gw.width())
	var gc = $(".grid-container");
	gc.height(size);
	gc.width(size);*/
});




var cells;
function CreateGrid(sideLength) {
	let size = sideLength * gridCellSize;
	let gc = $(".grid-container");
	gc.css("width", size+"em");
	gc.css("height", size+"em");
	$(".grid-cell").remove();
	cells = _.flatten(_.map(_.range(0,sideLength), y => _.map(_.range(0,sideLength), x => {
		let elem = $("<div></div>").addClass("grid-cell").css("left", (gridCellSize*x)+"em").css("top", (gridCellSize*y)+"em")
		.data("city-pos", new Vector(x,y)).data("grid-pos", new Vector(x,y));
		gc.append(elem);
		return elem;
	})), true);
}

function GenHeatColor(x, max) {
	// Spectral
	const maxColor = new util.Color(252, 110, 60);
	const midColor = new util.Color(255, 255, 191);
	const minColor = new util.Color(153, 213, 148);
	/*/
	const maxColor = new util.Color(255, 99, 72);
	const midColor = new util.Color(255, 190, 120);
	const minColor = new util.Color(230, 255, 200);
	//*/

	var midPoint = 0.25;
	var ratio = x/max;
	if (ratio < midPoint) {
		return util.MixColors(util.LinearScale(ratio, 0, midPoint, 0, 1), minColor, midColor).ToCSS();
	} else {
		return util.MixColors(util.LinearScale(ratio, midPoint, 1, 0, 1), midColor, maxColor).ToCSS();
	}
}

function DrawCity(c) {
	CreateGrid(c.sideLength);
	_.each($(".grid-cell").toArray(), cell => {
		let content = $(cell);
		let p = $(cell).data("grid-pos");
		let t = c.data[p.Coord(c.sideLength)];
		let md = p.ManhattanDistance(t);
		$(content).append($("<span></span>")
		.html("<div class='grid-cell-coords'>" + t.y + "," + t.x + "</div><div class='grid-cell-second'>" + md + "</div>"))
		.data("city-pos", t);
		$(cell).css("background-color", GenHeatColor(md, 2*c.sideLength-2));
	});
	shownCity = c;
}

function AnimateTransition(c, s) {
	let newCity = c.ApplyStep(s);
	_.each(s.transfers, transfer => {
		let cell = _.find(cells, x => $(x).data("grid-pos").Equals(transfer.from));
		$(cell).data("next-grid-pos", transfer.to);
		$(cell).css("left", (gridCellSize*transfer.to.x)+"em").css("top", (gridCellSize*transfer.to.y)+"em");
		let md = $(cell).data("city-pos").ManhattanDistance(transfer.to);
		$(cell).find("div.grid-cell-second").text(md.toString());
		$(cell).css("background-color", GenHeatColor(md, 2*c.sideLength-2));
	});
	_.each(cells, cell => {
		$(cell).data("grid-pos", $(cell).data("next-grid-pos"));
	});
	shownCity = newCity;
	return newCity;
}
