'use strict';

const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
let mainWindow;

function createWindow () {
	mainWindow = new BrowserWindow({width: 1200, height: 950});
	mainWindow.loadURL('file://' + __dirname + '/../index.html');
	//mainWindow.loadURL('https://www.google.de');
	console.log(__dirname);
	//mainWindow.webContents.openDevTools();

	mainWindow.on('closed', function() {
		mainWindow = null;
	});
}

app.on('ready', createWindow);

app.on('window-all-closed', function () {
	if (process.platform !== 'darwin') {
		app.quit();
	}
});

app.on('activate', function () {
	if (mainWindow === null) {
		createWindow();
	}
});
