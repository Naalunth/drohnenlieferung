#include "city.hpp"
#include <cmath>
#include <numeric>

City::City(uint32_t sideLength) :
	sideLength(sideLength),
	packetTargets(sideLength*sideLength)
{
}

auto v8String(const std::string& str) {
	return Nan::New(str).ToLocalChecked();
}


City::City(v8::Local<v8::Object>* jsCity)
{
	sideLength = Nan::To<uint32_t>(Nan::Get(*jsCity, v8String("sideLength")).ToLocalChecked()).FromJust();

	auto arr = Nan::Get(*jsCity, v8String("data")).ToLocalChecked().As<v8::Array>();
	auto len = (int32_t) arr->Length();
	packetTargets.reserve(len);
	for (int i = 0; i < len; i++) {
		auto val = Nan::Get(arr, i).ToLocalChecked().As<v8::Object>();
		auto x = Nan::To<int32_t>(Nan::Get(val, v8String("x")).ToLocalChecked()).FromJust();
		auto y = Nan::To<int32_t>(Nan::Get(val, v8String("y")).ToLocalChecked()).FromJust();
		packetTargets.push_back({ x,y });
	}
	
}

auto City::ApplyStep(const Step & step) -> City
{
	auto result = *this;
	for (auto& tr : step.transfers) {
		result.packetTargets[tr.second.ToIndex(sideLength)] = packetTargets[tr.first.ToIndex(sideLength)];
	}
	return result;
}

auto City::Deviation() -> uint32_t
{
	uint32_t result = 0;
	for (uint32_t i = 0; i < sideLength*sideLength; i++) {
		result += ManhattanDistance(packetTargets[i], Vector::FromIndex(i, sideLength));
	}
	return result;
}

Step::Step(uint32_t sideLength) :
	sideLength{ sideLength }
{
}

auto Step::AddTransfer(Transfer t) -> void
{
	if (t.first != t.second)
		transfers.push_back(t);
}

bool Step::operator==(const Step & other) const
{
	if (transfers.size() != other.transfers.size()) return false;
	std::sort(transfers.begin(), transfers.end());
	std::sort(other.transfers.begin(), other.transfers.end());
	auto first1 = transfers.begin(), first2 = other.transfers.begin();
	auto end1 = transfers.end();
	while (first1 != end1) 
		if (*first1++ != *first2++) return false;
	return true;
}

Vector Vector::FromIndex(size_t index, uint32_t sideLength)
{
	return{ (int32_t)index % (int32_t)sideLength, (int32_t)index / (int32_t)sideLength };
}

size_t Vector::ToIndex(uint32_t sideLength) const
{
	return x + sideLength * y;
}

bool Vector::operator==(const Vector& other) const
{
	return x==other.x && y==other.y;
}

bool Vector::operator!=(const Vector & other) const
{
	return !(*this == other);
}

bool Vector::operator<(const Vector & other) const
{
	if (y < other.y) return true;
	if (other.y < y) return false;
	if (x < other.x) return true;
	return false;
}

Vector Vector::operator+(const Vector & other) const
{
	return{ x + other.x, y + other.y };
}

Vector Vector::operator-(const Vector & other) const
{
	return{ x - other.x, y - other.y };
}

size_t hash_value(const Vector & v)
{
	size_t seed = 0;
	boost::hash_combine(seed, v.x);
	boost::hash_combine(seed, v.y);
	return seed;
}

uint32_t ManhattanDistance(const Vector & a, const Vector & b)
{
	return std::abs(a.x - b.x) + std::abs(a.y - b.y);
}
