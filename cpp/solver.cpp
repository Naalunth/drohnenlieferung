#include "solver.hpp"
#include <vector>
#include <queue>
#include <algorithm>
#include <cmath>
#include <future>
#include <iostream>
#include <boost/optional.hpp>


// Configurable stuff

const uint32_t numSamples = std::thread::hardware_concurrency() * 2;
//const uint32_t numSamples = 1;
const uint32_t numTrialsPerSample = 4;
const double beta = 0.2;
auto ExplorationSequence(uint32_t n) {
	return 0.2 * std::log(4.0 * std::log((double) n));
}




CitySolver::CitySolver(City * city) :
	city{ *city }, shouldContinueSearching(true), shouldRequeue(true), treeRoot{ nullptr, city, Step{} },
	rng{ std::random_device{}() }
{
}

CitySolver::~CitySolver()
{
}


auto CitySolver::CreateGoodStep() -> Step
{
	auto deviationBefore = city.Deviation();
	uint32_t bestDeviationSoFar = deviationBefore+100;
	uint32_t numTrials = 30;
	Step bestStep;
	bool redo = false;
	do {
		std::vector<std::future<Step>> futures;
		futures.reserve(numTrials);
		for (uint32_t i = 0; i < numTrials; i++) {
			futures.push_back(std::async(std::launch::async, [this](bool random) {
				StepConstraintResolver scr(city);
				scr.AddAsManyConstraintsAsPossible();
				return scr.GetResolution();
			}, i >= numTrials));
		}
		for (auto& fut : futures) {
			auto step = fut.get();
			auto newDev = city.ApplyStep(step).Deviation();
			if (newDev < bestDeviationSoFar) {
				bestDeviationSoFar = newDev;
				bestStep = step;
				numTrials = std::thread::hardware_concurrency() * 2;
			}
		}
	} while (redo);
	return bestStep;
}


auto CitySolver::ContinueSearching() -> std::vector<Step>
{
	static std::bernoulli_distribution dist(0.5);
	auto root = &treeRoot;
	shouldContinueSearching = true;
	while (shouldContinueSearching) {
		if (root->cityAfterStep.Deviation() == 0) {
			return std::vector<Step>();
		}

		// Descend down the tree
		auto node = root;
		for (;;) {
			node->timesVisited++;

			// Stop when we hit a leaf
			if ((node->timesVisited == 1 || node->shortestSolution == 0) && node != root) {
				break;
			}

			// Generate new nodes
			while (node->children.size() < 2.0 * pow((double) node->timesVisited, beta / 2.0)) {
				Step step;
				// assume there are no more steps after too many times
				int counter = 5;
				do {
					StepConstraintResolver scr(node->cityAfterStep);
					if (dist(rng)) scr.AddAsManyConstraintsAsPossible();
					else scr.AddRandomConstraints();
					step = scr.GetResolution();
				} while (std::any_of(node->children.begin(), node->children.end(), [&step](const std::unique_ptr<TreeNode>& n) {
					return n->step == step;
				}) && counter-- > 0);
				if (counter == 0) break;
				node->children.push_back(std::make_unique<TreeNode>(node, &node->cityAfterStep, step));
			}

			// Select a node
			auto nodeSelectCmp = [](const std::unique_ptr<TreeNode>& a, const std::unique_ptr<TreeNode>& b) {
				return a->UCB() < b->UCB();
			};
			auto it = std::max_element(node->children.begin(), node->children.end(), nodeSelectCmp);
			node = it->get();
			if (!shouldContinueSearching) break;
		}
		if (!shouldContinueSearching) break;

		// Sample the node a few times
		std::vector<double> samples;
		if (node->shortestSolution != 0) {
			auto res = SampleNode(node, numSamples);
			node->bestDescent = std::get<0>(res);
			std::reverse(node->bestDescent.begin(), node->bestDescent.end());
			node->shortestSolution = (uint32_t) std::get<0>(res).size();
			samples = std::get<1>(res);
		}
		if (!shouldContinueSearching) break;

		auto solution = node->bestDescent;
		auto shortestSolution = node->shortestSolution;
		double offset = 0.0;

		// Propagate the statistics up again
		while (node != root) {
			if (shortestSolution <= node->shortestSolution) {
				node->shortestSolution = shortestSolution;
				node->bestDescent = solution;
				solution.push_back(node->step);
			}
			for (auto& sample : samples) node->AddSample(sample, offset);
			offset++;
			shortestSolution = node->shortestSolution + 1;
			node = node->parent;
		}
		if (!shouldContinueSearching) break;


		// Report new best solution back to js
		for (auto& sample : samples) root->AddSample(sample, offset);
		if (shortestSolution < root->shortestSolution) {
			root->shortestSolution = shortestSolution;
			root->bestDescent = solution;
			return root->bestDescent;
		}

	}
	PrintTree(&treeRoot, 0);
	std::cout << "Visited nodes:      " << treeRoot.timesVisited << std::endl;
	std::cout << "Calculated samples: " << treeRoot.numSamples << std::endl;
	std::cout << "Generated steps:    ~" << numTrialsPerSample * treeRoot.samplesMean * treeRoot.numSamples << std::endl;
	std::cout << "Resolved conflicts: ~"
		<< 0.07 * std::pow(city.sideLength, 4.0) * numTrialsPerSample * treeRoot.samplesMean * treeRoot.numSamples
		<< std::endl << std::endl;
	return root->bestDescent;
}

auto CitySolver::StopSearching() -> void
{
	shouldContinueSearching = false;
	shouldRequeue = false;
}

auto CitySolver::SampleNode(TreeNode * node, uint32_t times) -> std::tuple<std::vector<Step>, std::vector<double>>
{
	std::vector<Step> bestSample;
	std::vector<double> samples;
	std::vector<std::future<std::vector<Step>>> sampleFutures;
	sampleFutures.reserve(times);
	for (uint32_t i = 0; i < times; i++) {
		sampleFutures.push_back(std::async(std::launch::async, &CitySolver::SampleNodeOnce, this, node));
	}
	bool first = true;
	for (uint32_t i = 0; i < times; i++) {
		auto sample = sampleFutures[i].get();
		if (first) {
			bestSample = sample;
			first = false;
		}
		samples.push_back((double)sample.size());
		if (sample.size() < bestSample.size())
			bestSample = sample;
	}
	return std::make_tuple(bestSample, samples);
}

auto CitySolver::SampleNodeOnce(TreeNode * node) -> std::vector<Step>
{
	std::vector<Step> steps;
	auto newCity = node->cityAfterStep;
	while (newCity.Deviation() > 0) {
		uint32_t bestDeviationSoFar = std::numeric_limits<uint32_t>::max();
		Step bestStep;
		for (uint32_t i = 0; i < numTrialsPerSample; i++) {
			StepConstraintResolver scr(newCity);
			scr.AddAsManyConstraintsAsPossible();
			auto step = scr.GetResolution();
			auto newDev = newCity.ApplyStep(step).Deviation();
			if (newDev < bestDeviationSoFar) {
				bestDeviationSoFar = newDev;
				bestStep = step;
			}
		}
		newCity = newCity.ApplyStep(bestStep);
		steps.push_back(bestStep);
	}
	return steps;
}


auto CitySolver::PrintTree(TreeNode * node, int indent) -> void
{
	std::string pre(indent, '\t');
	std::cout << pre << "Times visited: " << node->timesVisited << "\n";
	std::cout << pre << "Shortest:      " << node->shortestSolution << "\n";
	std::cout << pre << "Mean:          " << node->samplesMean << "\n";
	std::cout << pre << "Variance:      " << node->Variance() << "\n";
	std::cout << pre << "Value:         " << node->Value() << "\n";
	if (node != &treeRoot)
		std::cout << pre << "UCB:           " << node->UCB() << "\n";
	std::cout << pre << "Num children   " << node->children.size() << "\n";
	if (node->children.size() > 0)
		std::cout << pre << "Children: " << "\n";
	for (auto& child : node->children)
		PrintTree(child.get(), indent + 1);
	std::cout << "\n";
}



CitySolver::TreeNode::TreeNode(TreeNode* parent, City * city, Step step) :
	parent{ parent }, timesVisited { 0 }, step{ step },
	cityAfterStep{ city->ApplyStep(step) }, numSamples{ 0 },
	samplesMean{ 1.0 }, samplesM2{ 0.0 },
	shortestSolution{ std::numeric_limits<uint32_t>::max() }
{}

auto CitySolver::TreeNode::Value() const -> double
{
	double solutionDistance;
	if (timesVisited == 0) { // estimate value
		auto it = std::max_element(cityAfterStep.packetTargets.begin(), cityAfterStep.packetTargets.begin(),
			[](const Vector& a, const Vector& b) {
			return ManhattanDistance(a, { 0,0 }) < ManhattanDistance(b, { 0,0 });
		});
		solutionDistance = ManhattanDistance(*it, { 0,0 });
	}
	else {
		solutionDistance = (shortestSolution + samplesMean) / 2.0;
	}
	if (solutionDistance < 5.0 * (double) step.sideLength) return 1.0 - solutionDistance / (5.0 * (double) step.sideLength);
	return -1.0;
}

auto CitySolver::TreeNode::Variance() const -> double
{
	return samplesMean / (double) numSamples;
}

auto CitySolver::TreeNode::UCB() const -> double
{
	double ex = ExplorationSequence(parent->timesVisited);
	double n = 1.0 + (double) timesVisited;
	return Value() + std::sqrt(1.0 * Variance() * ex / n) + (ex / n);
}

auto CitySolver::TreeNode::AddSample(double x, double offset) -> void
{
	x += offset;
	numSamples++;
	auto oldM = samplesMean;
	if (numSamples == 1) {
		samplesMean = x;
		samplesM2 = 0.0;
	}
	else {
		samplesMean += (x - samplesMean) / numSamples;
		samplesM2 += (x - samplesMean)*(x - oldM);
	}
}



StepConstraintResolver::StepConstraintResolver(City city) :
	city(city),
	packets(city.sideLength*city.sideLength, Packet()),
	rng(std::random_device()())
{
	auto sl = city.sideLength;

	oldSlots.reserve(sl*sl);
	newSlots.reserve(sl*sl);
	for (uint32_t i = 0; i < sl*sl; i++) {
		oldSlots.emplace_back(Vector::FromIndex(i, sl), &packets[i]);
		newSlots.emplace_back(Vector::FromIndex(i, sl), &packets[i]);
	}
}

template <typename T> int sign(T val) {
	return (T(0) < val) - (val < T(0));
}

auto StepConstraintResolver::AddAsManyConstraintsAsPossible() -> void
{
	auto sl = city.sideLength;

	// Calculate some sensible steps
	typedef std::pair<Constraint, uint32_t> Entry;
	std::vector<Entry> transfers;
	for (size_t i = 0; i < sl*sl; i++) {
		auto pos = Vector::FromIndex(i, sl);
		auto v = city.packetTargets[i] - oldSlots[i].position;
		auto md = ManhattanDistance(v, { 0,0 });
		if (v.x != 0) transfers.push_back(std::make_pair(
			std::make_pair(pos, pos + Vector{ sign(v.x), 0 }), md));
		if (v.y != 0) transfers.push_back(std::make_pair(
			std::make_pair(pos, pos + Vector{ 0, sign(v.y) }), md));
	}
	if (transfers.size() == 0) return;

	// Add some variance and sort by priority
	std::shuffle(transfers.begin(), transfers.end(), rng);
	std::sort(transfers.begin(), transfers.end(), [](const Entry& a, const Entry& b) {
		return a.second > b.second;
	});

	// Apply (and filter) the constraints
	auto highestDistance = transfers[0].second;
	auto deviationInBeginning = city.Deviation();
	for (auto& transfer : transfers) {
		if (AddConstraint(transfer.first)) {
			auto newCity = city.ApplyStep(DoMakeStep());
			bool rollBack = false;
			if (newCity.Deviation() > deviationInBeginning) rollBack = true;
			// This check should remove any oscillating patterns
			if (newCity.Deviation() == deviationInBeginning) {
				for (size_t i = 0; i < sl*sl; i++) {
					if (ManhattanDistance(newCity.packetTargets[i], Vector::FromIndex(i, sl)) >= highestDistance
						&& ManhattanDistance(city.packetTargets[i], Vector::FromIndex(i, sl)) < highestDistance)
					{
						rollBack = true;
						break;
					}
				}
			}
			if (rollBack) {
				RemoveConstraint(transfer.first);
				DoRecalculateMapping();
			}
		} // if
	} // for
} // StepConstraintResolver::AddAsManyConstraintsAsPossible()


auto in_bounds(Vector v, int32_t max) {
	return v.x >= 0 && v.x < max && v.y >= 0 && v.y < max;
}

auto StepConstraintResolver::AddRandomConstraints() -> void
{
	// See AddAsManyConstraintsAsPossible
	// It's the same, just without any filtering and sorting
	auto sl = city.sideLength;

	typedef std::pair<Constraint, uint32_t> Entry;
	std::vector<Entry> transfers;
	transfers.reserve(sl*sl * 4);
	for (size_t i = 0; i < sl*sl; i++) {
		auto pos = Vector::FromIndex(i, sl);
		auto v = city.packetTargets[i] - oldSlots[i].position;
		if (v.x != 0 || v.y != 0) {
			for (auto& dir : { Vector{1,0}, Vector{-1,0}, Vector{0,1}, Vector{0,-1} })
				if (in_bounds(pos+dir, sl))
					transfers.push_back(std::make_pair(std::make_pair(pos, pos + dir), ManhattanDistance(v, dir)));
		}
	}
	if (transfers.size() == 0) return;
	std::shuffle(transfers.begin(), transfers.end(), rng);

	auto deviationInBeginning = city.Deviation();
	for (auto& transfer : transfers)
		AddConstraint(transfer.first);
}


auto StepConstraintResolver::AddConstraint(Constraint constraint) -> bool
{
	auto sl = city.sideLength;
	auto res = constraints.find(constraint);
	if (res != constraints.end()) return true;

	// Don't add if it conflicts with other constraints
	if (std::any_of(constraints.begin(), constraints.end(), [&constraint](Constraint c) {
		return c.first == constraint.first || c.second == constraint.second;
	})) return false;

	// Do the necessary transfer to fulfill the constraint
	auto constrainedPacket = oldSlots[constraint.first.ToIndex(sl)].formerPacket;
	auto emptyNewSlot = constrainedPacket->owner;
	auto displacedPacket = newSlots[constraint.second.ToIndex(sl)].packet;
	auto newlyFilledSlot = displacedPacket->owner;

	if (constrainedPacket != displacedPacket) {
		emptyNewSlot->packet = nullptr;
		newlyFilledSlot->packet = constrainedPacket;
		constrainedPacket->owner = newlyFilledSlot;

		displacedPacket->formerOwner->packet = displacedPacket;
		displacedPacket->owner = nullptr;
	}

	newlyFilledSlot->fixed = true;
	constrainedPacket->formerOwner->fixed = true;

	// Resolve the abandoned NewSlot
	if (TryFillNewSlot(emptyNewSlot)) {
		// Success
		constraints.insert(constraint);
		return true;
	}
	else {
		// Failure, roll back
		constrainedPacket->owner->fixed = false;
		constrainedPacket->formerOwner->fixed = false;
		TryFillNewSlot(emptyNewSlot);
		return false;
	}

}

auto StepConstraintResolver::RemoveConstraint(Constraint constraint) -> void
{
	auto it = constraints.find(constraint);
	if (it != constraints.end()) {
		constraints.erase(it);
		oldSlots[constraint.first.ToIndex(city.sideLength)].fixed = false;
		newSlots[constraint.second.ToIndex(city.sideLength)].fixed = false;
	}
}


auto GenerateManhattanSearchCircle(Vector v, uint32_t radius, uint32_t sl) {
	std::vector<Vector> result{ v };
	result.reserve(4 * radius + 1);
	for (int32_t i = 1; i <= (int32_t)radius; i++) {
		int32_t x = -i, y = 0, dx = 1, dy = -1;
		do {
			auto vec = v + Vector{x, y};
			if (in_bounds(vec, sl))
				result.push_back(vec);
			x += dx; y += dy;
			if (std::abs(x) == radius) dx = -dx;
			if (std::abs(y) == radius) dy = -dy;
		} while (x != -i);
	}
	return result;
}

auto StepConstraintResolver::TryFillNewSlot(NewSlot* slot) -> bool
{
	auto sl = city.sideLength;
	if (slot->packet != nullptr) return true;
	if (slot->fixed) return false;

	for (auto& ns : newSlots)
		ns.freeSearchMarked = false;

	struct SearchStruct
	{
		NewSlot* slot;
		std::vector<NewSlot*> path;
	};

	std::queue<SearchStruct> searchQueue;
	searchQueue.push({ slot, {} });

	while (searchQueue.size() > 0) {
		auto current = searchQueue.front();
		searchQueue.pop();

		if (current.slot->freeSearchMarked) continue;
		current.slot->freeSearchMarked = true;

		// Gather all old slots that could feed this new slot
		auto searchCircle = GenerateManhattanSearchCircle(current.slot->position, 1, sl);
		std::vector<OldSlot*> oldSlotsInSearchCircle;
		std::transform(searchCircle.begin(), searchCircle.end(), std::back_inserter(oldSlotsInSearchCircle),
			[this, &current, sl](const Vector& v) {
			return &oldSlots[v.ToIndex(sl)];
		});
		oldSlotsInSearchCircle.erase(std::remove_if(oldSlotsInSearchCircle.begin(), oldSlotsInSearchCircle.end(),
			[](const OldSlot* os) { return os->fixed; }), oldSlotsInSearchCircle.end());

		// Check if one of them has a packet ready
		std::vector<OldSlot*> maybeAvailablePackets;
		std::copy_if(oldSlotsInSearchCircle.begin(), oldSlotsInSearchCircle.end(), std::back_inserter(maybeAvailablePackets),
			[this, &current](const OldSlot* os) { return os->packet != nullptr; });

		if (maybeAvailablePackets.size() > 0) {
			auto it = std::find_if(maybeAvailablePackets.begin(), maybeAvailablePackets.end(), [this,sl](const OldSlot* os) {
				return newSlots[os->position.ToIndex(sl)].packet != nullptr;
			});
			OldSlot* source;
			// Prefer stealing from abandoned sources
			if (it != maybeAvailablePackets.end())
				source = *it;
			else
				source = maybeAvailablePackets[0];
			auto packet = current.slot->packet;
			current.slot->packet = source->packet;
			current.slot->packet->owner = current.slot;
			source->packet = nullptr;
			for (auto ns : current.path) {
				auto tmppkt = ns->packet;
				ns->packet = packet;
				ns->packet->owner = ns;
				packet = tmppkt;
			}
			return true;
		}

		// Ask the current owners of the packets for their packets
		std::vector<NewSlot*> newPath = { current.slot };
		newPath.insert(newPath.end(), current.path.begin(), current.path.end());
		for (auto os : oldSlotsInSearchCircle) {
			if (os->formerPacket->owner->freeSearchMarked) continue;
			searchQueue.push({ os->formerPacket->owner, newPath});
		}
	}

	return false;
}

auto StepConstraintResolver::GetResolution() -> Step
{
	DoRecalculateMapping();
	return DoMakeStep();
}

auto StepConstraintResolver::DoMakeStep() -> Step
{
	Step result(city.sideLength);
	for (auto& os : oldSlots)
		if (os.packet == nullptr)
			result.AddTransfer(std::make_pair(os.position, os.formerPacket->owner->position));
	return result;
}

auto StepConstraintResolver::DoRecalculateMapping() -> void
{
	auto sl = city.sideLength;
	// Undo all transfers first
	for (auto& os : oldSlots) {
		os.formerPacket->owner->packet = nullptr;
		os.packet = os.formerPacket;
		os.formerPacket->owner = nullptr;
	}

	// Reinstate constraints
	for (auto cons : constraints) {
		auto os = &oldSlots[cons.first.ToIndex(sl)];
		auto ns = &newSlots[cons.second.ToIndex(sl)];
		os->packet = nullptr;
		ns->packet = os->formerPacket;
		ns->packet->owner = ns;
	}

	// Then redo them
	for (auto& ns : newSlots) {
		TryFillNewSlot(&ns);
	}
}


StepConstraintResolver::Packet::Packet() :
	formerOwner(nullptr), owner(nullptr)
{
}

StepConstraintResolver::OldSlot::OldSlot(Vector position, Packet * packet) :
	position(position), packet(nullptr), formerPacket(packet), fixed(false)
{
	this->formerPacket->formerOwner = this;
}

StepConstraintResolver::NewSlot::NewSlot(Vector position, Packet* packet) :
	position(position), packet(packet), fixed(false), freeSearchMarked(false)
{
	this->packet->owner = this;
}

auto StepConstraintResolver::NewSlot::ClaimPacketFromOldSlot(OldSlot * os) -> void
{
}



// v8/libuv related stuff

CitySolutionWorker::CitySolutionWorker(Nan::Callback * cb, CitySolver* cs) :
	AsyncWorker(cb), cs(cs)
{
}

CitySolutionWorker::~CitySolutionWorker()
{
}

auto CitySolutionWorker::GetCitySolver() -> CitySolver *
{
	return cs;
}

void CitySolutionWorker::Execute()
{
	result = cs->ContinueSearching();
	std::reverse(result.begin(), result.end());
}

void CitySolutionWorker::HandleOKCallback()
{
	Nan::HandleScope scope;
	v8::Local<v8::Array> stepArray = Nan::New<v8::Array>((int) result.size());
	int i, j = 0;
	for (auto& step : result) {
		i = 0;
		v8::Local<v8::Array> transferArray = Nan::New<v8::Array>((int) step.transfers.size());
		for (auto& transfer : step.transfers) {
			auto from = Nan::New<v8::Object>();
			Nan::Set(from, Nan::New("x").ToLocalChecked(), Nan::New(transfer.first.x));
			Nan::Set(from, Nan::New("y").ToLocalChecked(), Nan::New(transfer.first.y));
			auto to = Nan::New<v8::Object>();
			Nan::Set(to, Nan::New("x").ToLocalChecked(), Nan::New(transfer.second.x));
			Nan::Set(to, Nan::New("y").ToLocalChecked(), Nan::New(transfer.second.y));
			auto obj = Nan::New<v8::Object>();
			Nan::Set(obj, Nan::New("from").ToLocalChecked(), from);
			Nan::Set(obj, Nan::New("to").ToLocalChecked(), to);

			Nan::Set(transferArray, i++, obj);
		}
		Nan::Set(stepArray, j++, transferArray);
	}

	v8::Local<v8::Value> argv[] = {
		Nan::Null(),
		stepArray,
		Nan::New(!cs->shouldRequeue)
	};

	callback->Call(3, argv);


	if (cs->shouldRequeue) {
		// steal the callback and requeue
		auto cb = callback;
		callback = nullptr;
		Nan::AsyncQueueWorker(new CitySolutionWorker(cb, cs));
	}
}
