#pragma once
#include <cstdint>
#include <vector>
#include <nan.h>
#include <boost/functional/hash.hpp>

struct Vector
{
	int32_t x;
	int32_t y;

	static Vector FromIndex(size_t index, uint32_t sideLength);
	size_t ToIndex(uint32_t sideLength) const;
	bool operator==(const Vector& other) const;
	bool operator!=(const Vector& other) const;
	bool operator<(const Vector& other) const;
	Vector operator+(const Vector& other) const;
	Vector operator-(const Vector& other) const;
	friend size_t hash_value(const Vector& v);
	friend uint32_t ManhattanDistance(const Vector& a, const Vector& b);
};

typedef std::pair<Vector, Vector> Transfer;

struct Step
{
public:
	Step() = default;
	Step(uint32_t sideLength);
	auto AddTransfer(Transfer t) -> void;
	bool operator==(const Step& other) const;

	uint32_t sideLength;
	mutable std::vector<Transfer> transfers;
};

struct City
{
public:
	City(uint32_t sideLength);
	City(v8::Local<v8::Object>* jsCity);

	auto ApplyStep(const Step& step)->City;
	auto Deviation()->uint32_t;

	uint32_t sideLength;
	std::vector<Vector> packetTargets;
};

