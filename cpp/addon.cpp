#define NOMINMAX
#include <nan.h>
#include "city.hpp"
#include "solver.hpp"

v8::Persistent<v8::Object> solutionWorkerCallbackProto;
v8::Persistent<v8::ObjectTemplate> solutionWorkerCallbackTemplate;


NAN_METHOD(StartSolutionWorker) {
	auto cityPar = info[0].As<v8::Object>();
	auto c = new City(&cityPar);
	auto cb = new Nan::Callback(info[1].As<v8::Function>());

	auto cs = new CitySolver(c);
	auto csw = new CitySolutionWorker(cb, cs);
	Nan::AsyncQueueWorker(csw);

	auto instance = Nan::New(solutionWorkerCallbackTemplate)->NewInstance();
	Nan::SetPrototype(instance, Nan::New(solutionWorkerCallbackProto));
	Nan::SetInternalFieldPointer(instance, 0, csw->GetCitySolver());
	info.GetReturnValue().Set(instance);
}

NAN_METHOD(StopSolutionWorker) {
	auto cs = (CitySolver*) Nan::GetInternalFieldPointer(info.This(), 0);
	cs->StopSearching();
}


NAN_MODULE_INIT(Init) {
	auto proto = Nan::New<v8::Object>();
	Nan::SetMethod(proto, "Stop", StopSolutionWorker);
	solutionWorkerCallbackProto.Reset(target->GetIsolate(), proto);
	auto tmpl = Nan::New<v8::ObjectTemplate>();
	tmpl->SetInternalFieldCount(1);
	solutionWorkerCallbackTemplate.Reset(target->GetIsolate(), tmpl);

	Nan::SetMethod(target, "StartSolutionWorker", StartSolutionWorker);
}

NODE_MODULE(addon, Init)
