#pragma once
#define NOMINMAX
#include <memory>
#include <unordered_set>
#include <vector>
#include <queue>
#include <random>
#include <atomic>
#include <nan.h>
#include <boost/optional.hpp>
#include <boost/functional/hash.hpp>
#include "city.hpp"

typedef Transfer Constraint;

class CitySolver
{
private:
	friend class StepConstraintResolver;
	struct TreeNode;
public:
	CitySolver(City* city);
	~CitySolver();

	auto CreateGoodStep()->Step;

	auto ContinueSearching()->std::vector<Step>;
	auto StopSearching()->void;
	
	std::atomic_bool shouldRequeue;
private:
	auto SampleNode(TreeNode* node, uint32_t times)->std::tuple<std::vector<Step>, std::vector<double>>;
	auto SampleNodeOnce(TreeNode* node)->std::vector<Step>;
	auto PrintTree(TreeNode* node, int indent)->void;

	struct TreeNode
	{
		uint32_t timesVisited;
		uint32_t shortestSolution;
		uint32_t numSamples;
		double samplesMean;
		double samplesM2;

		TreeNode* parent;
		City cityAfterStep;
		Step step;
		std::vector<std::unique_ptr<TreeNode>> children;
		std::vector<Step> bestDescent;

		TreeNode(TreeNode* parent, City* city, Step step);
		auto Value() const->double;
		auto Variance() const->double;
		auto UCB() const->double;
		auto AddSample(double x, double offset) ->void;
	};

	City city;
	TreeNode treeRoot;
	std::atomic_bool shouldContinueSearching;
	std::mt19937 rng;
};


class StepConstraintResolver
{
private:
	struct OldSlot;
	struct NewSlot;
	struct Packet;

public:
	StepConstraintResolver(City city);

	auto AddAsManyConstraintsAsPossible()->void;
	auto AddRandomConstraints()->void;

	auto AddConstraint(Constraint constraint)->bool;
	auto RemoveConstraint(Constraint constraint)->void;

	auto GetResolution()->Step;

private:
	auto TryFillNewSlot(NewSlot* slot)->bool;
	auto DoMakeStep()->Step;
	auto DoRecalculateMapping()->void;

	struct Packet
	{
		Packet();
		OldSlot* formerOwner;
		NewSlot* owner;
	};
	struct OldSlot
	{
		OldSlot(Vector position, Packet* packet);
		Vector position;
		Packet* packet;
		Packet* formerPacket;
		bool fixed;
	};
	struct NewSlot
	{
		NewSlot(Vector position, Packet* packet);
		auto ClaimPacketFromOldSlot(OldSlot* os) -> void;
		Vector position;
		Packet* packet;
		bool fixed;
		bool freeSearchMarked;
	};

	std::vector<Packet> packets;
	std::vector<OldSlot> oldSlots;
	std::vector<NewSlot> newSlots;

	std::unordered_set<Constraint, boost::hash<Constraint>> constraints;

	std::mt19937 rng;

	City city;
};



// v8/libuv related stuff

class CitySolutionWorker : public Nan::AsyncWorker
{
public:
	CitySolutionWorker(Nan::Callback* cb, CitySolver* cs);

	~CitySolutionWorker();

	auto GetCitySolver()->CitySolver*;

	auto Execute()->void;

	auto HandleOKCallback()->void;

private:
	CitySolver* cs;
	std::vector<Step> result;
};
